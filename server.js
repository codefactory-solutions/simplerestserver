// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var passport = require('passport');
var passportConfig = require('./app/config/passport.js')(passport);


// configure app
var port = process.env.PORT || 3333; // set our port
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/rest'); // connect to our database

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();
require('./routes.js')(app, passport, router);

// START THE SERVER
// =============================================================================
app.listen(port, '192.168.1.141');
console.log('Magic happens on port ' + port);