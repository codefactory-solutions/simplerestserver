var express = require('express');
var passport = require('passport');
var cors = require('cors');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var Item = require('./app/modules/item');
var ItemController = require('./app/controllers/item');
var Category = require('./app/modules/category');

var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname+ '-' + Date.now()+'.jpg')
    }
});
var upload = multer({ storage: storage });



module.exports = function(app, passport, router) {

    // middleware to use for all requests
    app.use(morgan('dev')); // log requests to the console
    app.use(bodyParser.json());
    app.use(cors());


    router.route('/multer')
        .get( function(req, res) {
            res.json('use post');
        })
        .post(upload.single('imagefile'), function(req, res) {
            res.json('ok');
        });

    // on routes that end in /categories
    // ----------------------------------------------------
    router.route('/categories')
        .get(Category.get) // get all the items (accessed at GET http://localhost:3333/api/categories)
        // on routes that end in /items
        // ----------------------------------------------------
    router.route('/items')
        .post(ItemController.create) // create a item (accessed at POST http://localhost:3333/api)
        .get(passport.isAuthenticated, passport.isAdmin, ItemController.index); // get all the items (accessed at GET http://localhost:3333/api/items)
    // on routes that end in /items/:item_id
    // ----------------------------------------------------
    router.route('/items/:id')
        .get(Item.find) // get the item with that id
        .put(Item.update) // update the item with this id
        .delete(Item.delete); // delete the item with this id
    app.use('/api', router);
};