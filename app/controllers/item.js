var Item = require('../modules/item');

var index = function(req, res) {
    Item.index(function(err, response){
    	if(err) res.send(err);
    	res.json(response);
    });
};


var create = function(req, res) {
    Item.create(req.body, function(err, response){
    	if(err) res.send(err);
    	res.json(response);
    }, req.file.filename);
};

module.exports = {
	index : index,
	create : create
}