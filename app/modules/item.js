var Item = require('../models/item');

/**
 * [get Returns all items]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
exports.index = function(callback) {
    Item.find().populate('_category').exec(function(err, items) {
        if (err) callback(err, null);
        callback(null, items);
    });
};

/**
 * [find Finds and returns a specific item based on a given id]
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
exports.find = function(req, res) {
    Item.findById(req.params.id, function(err, item) {
        if (err) res.send(err);
        res.json(item);
    });
};

exports.create = function(data, callback, filename) {
    if(filename){
       data.filename = filename; 
    }
    
    Item.create(data, function(err, item) {
        if (err) callback(err, null);
        callback(null, item);
    });
};

exports.update = function(req, res) {
    Item.findOneAndUpdate({
        _id: req.params.id
    }, req.body, {}, function(err, item) {
        if (err) res.send(err);
        res.json(item);
    });
};

exports.delete = function(req, res) {
    Item.remove({
        _id: req.params.id
    }, function(err, item) {
        if (err) res.send(err);
        res.json(req.params.id);
    });
};