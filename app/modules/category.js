var Category = require('../models/category');
exports.get = function(req, res) {
    Category.find(function(err, categories) {
        if (err) res.send(err);
        res.json(categories);
    });
};