var mongoose		= require('mongoose');
var Schema 			= mongoose.Schema;
var Category		= require('./category');

var ItemSchema	= new Schema({
	title: String,
	_category : { type: Schema.Types.ObjectId, ref: 'Category' },
	picUrl: String
});

// Duplicate the ID field.
ItemSchema.virtual('id').get(function(){
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
ItemSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Item', ItemSchema);