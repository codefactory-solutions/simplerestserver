var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CategorySchema   = new Schema({
	title: String
});

// Duplicate the ID field.
CategorySchema.virtual('id').get(function(){
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
CategorySchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Category', CategorySchema);