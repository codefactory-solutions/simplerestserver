// config/passport.js
// load all the things we need
var BearerStrategy = require('passport-http-bearer').Strategy;
var User = require('../models/user');
module.exports = function(passport) {
    // =========================================================================
    // passport setup ==================================================
    // =========================================================================
    passport.isAuthenticated = passport.authenticate('bearer', {
        session: false,
        failureRedirect: '/api/unauthorized'
    });

    passport.isAdmin = function(req, res, next) {
        if (req.authInfo.scope != 'admin') {
            res.statusCode = 403;
            return res.end('Forbidden');
        }
        return next();
    };

    passport.use(new BearerStrategy(function(token, done) {
        User.findOne({
            token: token
        }, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false);
            }
            return done(null, user, {
                scope: user.scope
            });
        });
    }));
};